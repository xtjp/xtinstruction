//
//  ViewController.swift
//  Demo
//
//  Created by 闻端 on 16/8/15.
//  Copyright © 2016年 xiaotaojiang. All rights reserved.
//

import UIKit
import XTInstruction
class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let instruction = XTInstruction(
        pieces: [
            XTInstructionPiece(prompt: "Hello", targetRect: CGRect(x: 10, y: 10, width: 100, height: 100)),
            XTInstructionPiece(prompt: "World", targetRect: CGRect(x: 100, y: 200, width: 50, height: 50))
        ],
        identifier: "Demo",
        style: XTInstructionStyle.dark,
        animationStyle: .scale
    )
    instruction.showOn(self, force: true)
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

