# XTInstruction

![](http://o91sw003p.bkt.clouddn.com/57490166_p0.png)

## Install
Use Carthage: 

```
// Swift 3
git "https://bitbucket.org/xtjp/xtinstruction" 
// Swift 2.2
git "https://bitbucket.org/xtjp/xtinstruction" "swift2.2"
```

CocoaPods:

```
// Swift 3
pod 'XTInstruction', :git=>'https://bitbucket.org/xtjp/xtinstruction', :branch => 'master'
// Swift 2.2
pod 'XTInstruction', :git=>'https://bitbucket.org/xtjp/xtinstruction', :tag => 'swift2.2'

```

##Usage

### Initialize an instruction: 

```swift 
let instruction = XTInstruction(pieces: [
    XTInstructionPiece(prompt: "First", targetRect: CGRect(x: 10, y: 100, width: 100, height: 100)),
    XTInstructionPiece(prompt: "Second", targetRect: CGRect(x: 10, y: 300, width: 100, height: 100))
    ], identifier: "Demo Presentation", style: .Light)
```
![](http://o91sw003p.bkt.clouddn.com/demo_light_dark.png)


### In Your ViewController:

Show it once in App per identifier:

```swift 
instruction.showOn(self)
```

Force it to show: 

```swift
instruction.showOn(self, force: true)
```

Do other executions when on certain index: 

```swift
instruction.executionBlock = { index:Int in 
    // do something 
}
```