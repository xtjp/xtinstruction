//
//  XTInstructionPiece.swift
//  XTInstruction
//
//  Created by 闻端 on 16/6/20.
//  Copyright © 2016年 xiaotaojiang. All rights reserved.
//

import UIKit

open class XTInstructionPiece: NSObject {
	let prompt: String
	let targetRect: CGRect
	public init(prompt: String, targetRect: CGRect){
		self.prompt = prompt
		self.targetRect = targetRect
	}
}
