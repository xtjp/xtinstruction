//
//  XTInstruction.swift
//  XTInstruction
//
//  Created by 闻端 on 16/6/20.
//  Copyright © 2016年 xiaotaojiang. All rights reserved.
//

import UIKit

public enum  XTInstructionStyle {
	case dark,light,deepDark
}

public enum XTInstructionAnimationStyle {
	case none, float, scale
}

public final class XTInstruction: UIViewController {
	let instructionPieces: Array<XTInstructionPiece>
	fileprivate let identifier: String
	fileprivate let style:XTInstructionStyle
	fileprivate let animationStyle: XTInstructionAnimationStyle
	// UI
	let overlayLayer: CALayer = CALayer()
	let overlayLayerMask: CAShapeLayer = CAShapeLayer()
	// state
	var index: Int = 0
	var lastPopoverView: UIView?
	
	// execution
	public typealias XTInstructionExecutionBlock = (_ index:Int) -> ()
	var executionBlock:XTInstructionExecutionBlock?
  let completion: (() -> ())?
	
  public init(pieces: Array<XTInstructionPiece>, identifier: String , style: XTInstructionStyle = .light, animationStyle: XTInstructionAnimationStyle = .none, completion: (() -> ())? = nil){
		self.instructionPieces = pieces
		self.identifier = identifier
		self.style = style
		self.animationStyle = animationStyle
    self.completion = completion
		super.init(nibName: nil, bundle: nil)
		self.view.backgroundColor = UIColor.clear
		self.modalPresentationStyle = .overCurrentContext
		self.modalTransitionStyle = .crossDissolve
	}
	
	required public init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public func showOn(_ viewController: UIViewController, force:Bool = false) {
		if force == true {
			viewController.present(self, animated: true, completion: nil)
			return
		}
		if XTInstructionManager.showOrNot(self.identifier) {
			XTInstructionManager.registerID(self.identifier, withDate: Date())
			viewController.present(self, animated: true, completion: nil)
		}
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		self.view.layer.addSublayer(self.overlayLayer)
		self.overlayLayer.mask = self.overlayLayerMask
		if self.style == .light {
			self.overlayLayer.backgroundColor = UIColor(white: 1, alpha: 0.8).cgColor
		} else {
			self.overlayLayer.backgroundColor = UIColor(white: 0, alpha: 0.4).cgColor
		}
		self.overlayLayer.frame = UIScreen.main.bounds
		self.overlayLayerMask.fillRule = kCAFillRuleEvenOdd
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(XTInstruction.handleTap(_:)))
		self.view.addGestureRecognizer(tapGesture)
	}
	
	override public func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.nextPresentation()
	}
	
	@objc fileprivate func handleTap(_ tapGesture: UITapGestureRecognizer){
		if tapGesture.state != .ended {return}
		self.nextPresentation()
	}
	
	fileprivate func nextPresentation(){
		if self.index == self.instructionPieces.count {
      let completion = self.completion
      self.dismiss(animated: true, completion: {
        completion?()
      })
			return
		}
		nextMask()
		dismissLastPopOverView()
		nextPopOver()
		executionBlock?(self.index)
		index += 1
	}
	
	fileprivate func nextMask(){
		let instruction = self.instructionPieces[index]
		let maskPath = CGMutablePath()
		maskPath.addRect(self.overlayLayer.frame)
		let targetFrame = instruction.targetRect
		maskPath.__addRoundedRect(transform: nil, rect: targetFrame.insetBy(dx: -3, dy: -3), cornerWidth: 4, cornerHeight: 4)
		self.overlayLayerMask.path = maskPath
	}
	
	fileprivate func nextPopOver(){
		let instructionPiece = self.instructionPieces[index]
		let popoverView = UIView()
		self.lastPopoverView = popoverView
		let backgroundView = UIView()
		let textView = UITextView()
		let backgroundViewMaskLayer = CAShapeLayer()
		self.view.addSubview(popoverView)
		popoverView.addSubview(backgroundView)
		popoverView.addSubview(textView)
		backgroundView.layer.mask = backgroundViewMaskLayer
		
		// Decoration
		let backgroundColor:UIColor
		if self.style == .light || self.style == .deepDark {
			backgroundColor = UIColor(white: 0, alpha: 0.8)
		} else {
			backgroundColor = UIColor(white: 1, alpha: 0.8)
		}
		backgroundView.backgroundColor = backgroundColor
		
		textView.text = instructionPiece.prompt
		if self.style == .light || self.style == .deepDark {
			textView.textColor = UIColor.white
		} else {
			textView.textColor = UIColor.black
		}
		textView.backgroundColor = UIColor.clear
		textView.layer.cornerRadius = 4
		textView.font = UIFont.systemFont(ofSize: 14)
		textView.textAlignment = .left
		textView.layer.shadowOpacity = 0.2
		textView.layer.shadowOffset = CGSize(width: 0, height: 1)
		textView.isUserInteractionEnabled = false
		
		// Step1: get textView & popoverView Rect
		let textViewSize = textView.sizeThatFits(CGSize(width: self.view.bounds.width - 60, height: self.view.bounds.height - 60))
		textView.bounds = CGRect(x: 0, y: 0, width: textViewSize.width, height: textViewSize.height)
		let topBottomInset: CGFloat = 3
		let leftRightInset: CGFloat = 5
		let triangleAreaHeight: CGFloat = 12
		let popoverSize = CGSize(width: textViewSize.width + leftRightInset * 2, height: textViewSize.height + topBottomInset * 2 + triangleAreaHeight)
		
		var popoverRect = CGRect(x: instructionPiece.targetRect.midX - popoverSize.width / 2, y: instructionPiece.targetRect.maxY, width: popoverSize.width, height: popoverSize.height)
		var triangleAtTop = true
		// move bottom => top
		if popoverRect.maxY >= self.view.bounds.height - 10 {
			triangleAtTop = false
			popoverRect.origin.y = instructionPiece.targetRect.minY - popoverRect.height
		}
		// ensure not exceed left & right bounds
		if popoverRect.minX <= 10 {
			popoverRect.origin.x = 10
		}
		if popoverRect.maxX >= self.view.bounds.width - 10 {
			popoverRect.origin.x = self.view.bounds.width - 10 - popoverRect.width
		}
		popoverView.frame = popoverRect
		backgroundView.frame = CGRect(x: 0, y: 0, width: popoverRect.width, height: popoverRect.height)
		// Step2: Mask. & layout
		let triangleBorderWidth: CGFloat = 12
		
		let roundedRectLayer = CALayer()
		roundedRectLayer.backgroundColor = UIColor.black.cgColor
		roundedRectLayer.cornerRadius = 4
		let triangleLayer = CALayer()
		triangleLayer.bounds = CGRect(x: 0, y: 0, width: triangleBorderWidth, height: triangleBorderWidth)
		triangleLayer.transform = CATransform3DMakeRotation(CGFloat(M_PI) / 4, 0, 0, 1)
		triangleLayer.backgroundColor = UIColor.black.cgColor
		backgroundViewMaskLayer.addSublayer(roundedRectLayer)
		backgroundViewMaskLayer.addSublayer(triangleLayer)
		
		var positionX = self.view.convert(CGPoint(x: instructionPiece.targetRect.midX, y: 0), to: popoverView).x
		let positionY: CGFloat
		if triangleAtTop {
			roundedRectLayer.frame = CGRect(x: 0, y: triangleAreaHeight, width: popoverSize.width, height: popoverSize.height - triangleAreaHeight)
			textView.center = CGPoint(x: popoverRect.width / 2, y: (popoverRect.height + triangleAreaHeight) / 2)
			positionY = triangleAreaHeight
		} else {
			roundedRectLayer.frame = CGRect(x: 0, y: 0, width: popoverSize.width, height: popoverSize.height - triangleAreaHeight)
			textView.center = CGPoint(x: popoverRect.width / 2, y: (popoverRect.height - triangleAreaHeight) / 2)
			positionY = popoverRect.height - triangleAreaHeight
		}
		// 设计上的妥协 ( 缩进 )
		if positionX < 14 { positionX += 5 }
		if positionX > popoverView.frame.width - 14 { positionX -= 5 }
		triangleLayer.position = CGPoint(x: positionX, y: positionY)
		
		// Step3: Animation.
		presentPopOverView(popoverView, atBottom: triangleAtTop)
	}
	
	fileprivate func presentPopOverView(_ popoverView: UIView, atBottom: Bool) {
		if self.animationStyle == .none {
			popoverView.alpha = 0
			UIView.animate(
				withDuration: 0.3,
				animations: {
					popoverView.alpha = 1
				}
			)
			return
		}
		if self.animationStyle == .float {
			popoverView.alpha = 0
			let offset: CGFloat = atBottom ? 16 : -16
			popoverView.layer.transform = CATransform3DMakeTranslation(0, offset, 0)
			UIView.animate(
				withDuration: 0.5,
				delay: 0,
				options: [.repeat, .autoreverse],
				animations: {
					popoverView.layer.transform = CATransform3DMakeTranslation(0, offset / 2, 0)
				},
				completion: nil
			)
			UIView.animate(
				withDuration: 0.3,
				animations: {
					popoverView.alpha = 1
				}
			)
			return
		}
		if self.animationStyle == .scale {
			popoverView.alpha = 1
			popoverView.layer.transform = CATransform3DMakeScale(0, 0, 0)
			UIView.animate(
				withDuration: 0.5,
				delay: 0,
				usingSpringWithDamping: 0.5,
				initialSpringVelocity: 0,
				options: [],
				animations: {
					popoverView.layer.transform = CATransform3DMakeScale(1, 1, 1)
				},
				completion: nil
			)
			return
		}
	}
	
	fileprivate func dismissLastPopOverView(){
		guard let thisView = self.lastPopoverView else { return }
		self.lastPopoverView = nil
		// last instructionPiece move-out animation
		UIView.animate(withDuration: 0.3, animations: {
			thisView.alpha = 0
			}, completion: { (finished: Bool) in
				thisView.removeFromSuperview()
		})
	}
}
