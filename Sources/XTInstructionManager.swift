//
//  XTInstructionManager.swift
//  XTInstruction
//
//  Created by 闻端 on 16/6/20.
//  Copyright © 2016年 xiaotaojiang. All rights reserved.
//

import Foundation

final class XTInstructionManager: NSObject {
	static let sharedManager = XTInstructionManager()
	fileprivate override init() {
		super.init()
	}
	class func defaultsKeyForID(_ identifier: String) -> String{
		return "XTInstructor.\(identifier).lastShown"
	}
	class func lastShownForInstructionWithID(_ identifier: String) -> Date?{
		let defaults = UserDefaults.standard
		if let date = defaults.object(forKey: XTInstructionManager.defaultsKeyForID(identifier)) as? Date{
			return date
		}
		return nil
	}
	class func showOrNot(_ identifier: String) -> Bool{
		let date = XTInstructionManager.lastShownForInstructionWithID(identifier)
		if date == nil {
			return true
		} else {
			return false
		}
	}
	class func registerID(_ identifier: String, withDate date: Date){
		let defaults = UserDefaults.standard
		defaults.set(date, forKey: XTInstructionManager.defaultsKeyForID(identifier))
	}
}
